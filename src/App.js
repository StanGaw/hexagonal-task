import "./App.css";
import Form from "./components/form/Form";
import Header from "./components/header/Header";
function App() {
  return (
    <div>
      <Header>Dish post app</Header>
      <Form />
    </div>
  );
}

export default App;
