import React, { useState, useEffect } from "react";
import Select from "../selects/Select";
import { formValidation } from "./formValidation";
import { dishPost } from "../../functions/api";
import { useSwitch } from "../selects/useSwitch";
import types from "../types/types";
import styles from "./styles.module.css";
const { PIZZA, SOUP, SANDWICH } = types;
const initialState = {
  name: "",
  preparation_time: "",
  type: "",
};
const Form = () => {
  const [type, setType] = useState();
  const [form, setForm] = useState(initialState);
  const [validate, setValidate] = useState(false);
  const [error, setError] = useState("");
  const [message, setMesasge] = useState();
  const dishLabels = [PIZZA, SOUP, SANDWICH];
  const typeObject = useSwitch(type);

  const typeHandler = (e) => {
    setType(e.target.value);
    formHanlder(e);
  };
  const formHanlder = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const fetchPost = () => {
    dishPost(form)
      .then((rsp) => {
        if (rsp.status === 200) {
          setMesasge("Success!!!");
          return rsp.json();
        } else {
          setMesasge("Something went wrong");
        }
      })
      .then((data) => console.log(data));
  };
  const submitForm = (e) => {
    e.preventDefault();
    formValidation(e, form, typeObject, setError, setValidate);
  };

  useEffect(() => {
    validate && fetchPost();
  }, [validate]);

  return (
    <form className={styles.form} onSubmit={(e) => submitForm(e)}>
      <label>
        <p>Name</p>
        <input onChange={(e) => formHanlder(e)} name="name"></input>
      </label>
      <label>
        <p>Preparation time</p>
        <input onChange={(e) => formHanlder(e)} name="preparation_time"></input>
      </label>

      <label>
        <p>Type</p>
        <select onChange={(e) => typeHandler(e)} value={type} name="type">
          <option key={null}>--</option>
          {dishLabels.map((item) => {
            return <option key={item}>{item}</option>;
          })}
        </select>
      </label>
      <Select type={type} onChange={(e) => formHanlder(e)} />
      <button className={styles.button} type="submit">
        Submit
      </button>
      <div>
        {error?.length > 0 &&
          error?.map((el) => {
            return <p key={el}>{el}</p>;
          })}
      </div>
      <p>{message}</p>
    </form>
  );
};

export default Form;
