export const formValidation = (e, form, typeObject, setError, setValidate) => {
  e.preventDefault();
  const preparation_timeRegex = /\d\d:\d\d:\d\d/g;
  const nameRegex = /(?!^\d+$)^.+$/g;
  const { name, preparation_time, type } = form;
  const message = [];
  if (!nameRegex.test(name)) message.push("name is required");
  if (!preparation_timeRegex.test(preparation_time))
    message.push("preparation time requires hh:mm:ss format");
  if (type === "") message.push("type is required");
  if (type !== "") {
    const specialFields = typeObject?.fields;

    specialFields.map((el) => {
      if (form[el.label] === undefined) {
        message.push(`${el.label} is required`);
      }

      if (el.units === "number" && isNaN(form[el.label])) {
        message.push(`${el.label} need to be a number`);
      }
      if (el.units === "string" && !isNaN(form[el.label])) {
        message.push(`${el.label} need to be a string`);
      }
    });
  }

  if (message.length > 0) {
    setError(message);
  } else {
    setError([]);
    setValidate(true);
  }
};
