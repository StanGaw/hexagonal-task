import React from "react";
import { useSwitch } from "./useSwitch";

const Select = ({ type, onChange }) => {
  const typeObject = useSwitch(type);
  const select = (typeObject) => {
    const fields = typeObject?.fields;
    return fields?.map((field) => {
      if (field?.type === "input") {
        return (
          <label key={field?.label}>
            <p>{field.title}</p>
            <input onChange={(e) => onChange(e)} name={field?.label}></input>
          </label>
        );
      }

      if (field?.type === "select") {
        return (
          <label key={field?.label}>
            <p>{field.title}</p>
            <select
              onChange={(e) => onChange(e)}
              key={field?.label}
              name={field?.label}
            >
              <option key={null}>--</option>
              {field?.options.map((value) => {
                return <option key={value}>{value}</option>;
              })}
            </select>
          </label>
        );
      }
      return null;
    });
  };

  if (type === undefined) {
    return null;
  } else {
    return select(typeObject);
  }
};

export default Select;
