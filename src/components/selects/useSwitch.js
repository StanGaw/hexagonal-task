import typeObjects from "../types/typeObjects";
import types from "../types/types";
const { PIZZA, SANDWICH, SOUP } = types;
const { PIZZA_OBJ, SANDWICH_OBJ, SOUP_OBJ } = typeObjects;

export const useSwitch = (type = null) => {
  switch (type) {
    case PIZZA:
      return PIZZA_OBJ;
    case SANDWICH:
      return SANDWICH_OBJ;
    case SOUP:
      return SOUP_OBJ;
    default:
      return null;
  }
};
