import types from "./types";
const {
  PIZZA,
  SOUP,
  SANDWICH,
  NO_OF_SLICES,
  DIAMETER,
  SPICINESS_SCALE,
  SLICES_OF_BREAD,
} = types;
const PIZZA_OBJ = {
  name: PIZZA,
  fields: [
    {
      title: "Number of Slices",
      label: NO_OF_SLICES,
      type: "input",
      units: "number",
    },
    {
      title: "Pizza diameter",
      label: DIAMETER,
      type: "input",
      units: "number",
    },
  ],
};
const SOUP_OBJ = {
  name: SOUP,
  fields: [
    {
      title: "Scale of spiciness",
      label: SPICINESS_SCALE,
      type: "select",
      units: "number",
      options: [1, 2, 3, 4, 5, 6, 7, 8, 9],
    },
  ],
};
const SANDWICH_OBJ = {
  name: SANDWICH,
  fields: [
    {
      title: "Number of Slices",
      label: SLICES_OF_BREAD,
      type: "input",
      units: "number",
    },
  ],
};

export default { PIZZA_OBJ, SOUP_OBJ, SANDWICH_OBJ };
