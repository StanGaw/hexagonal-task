const PIZZA = "pizza";
const SOUP = "soup";
const SANDWICH = "sandwich";
const NO_OF_SLICES = "no_of_slices";
const DIAMETER = "diameter";
const SPICINESS_SCALE = "spiciness_scale";
const SLICES_OF_BREAD = "slices_of_bread";
export default {
  PIZZA,
  SANDWICH,
  SOUP,
  NO_OF_SLICES,
  DIAMETER,
  SPICINESS_SCALE,
  SLICES_OF_BREAD,
};
