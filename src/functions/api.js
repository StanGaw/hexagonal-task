export async function dishPost(dish) {
  const tab = Object.keys(dish);
  let obj = {};
  tab.map((el) => {
    if (
      el === "no_of_slices" ||
      el === "spiciness_scale" ||
      el === "slices_of_bread"
    ) {
      obj = {
        ...obj,
        [el]: parseInt(dish[el]),
      };
    } else if (el === "diameter") {
      obj = {
        ...obj,
        [el]: parseFloat(dish[el]),
      };
    } else {
      obj = {
        ...obj,
        [el]: dish[el],
      };
    }
  });

  const url = "https://frosty-wood-6558.getsandbox.com:443/dishes";
  const response = await fetch(url, {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
    },
    redirect: "follow",
    referrerPolicy: "no-referrer",
    body: JSON.stringify(obj),
  });

  return response;
}
